@extends('layout')
@section('content')
    <div class="container">
        <h3>Create new review</h3>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route'=> ['store']]) !!}
                <div class="form-group">
                    <h5>Vote from one to ten</h5>
                    <input type="number" class="form-control" name="vote">
                    <br>
                    <h5>Leave all your comments here</h5>
                    <textarea name="review" id="" cols="30" rows="10" class="form-control"></textarea>
                    <br>
                    <button class="btn btn-success">Submit review</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
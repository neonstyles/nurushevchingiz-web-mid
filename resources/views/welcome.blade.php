@extends('layout')
@section('content')
    <div class="container">
        <h3>Reviews</h3>
        <div class="row">
            <a href="{{ route('create') }}" class="btn btn-success">Create new Review</a>
            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <thead>
                        <tr>
                         <td>ID</td>
                        <td>Vote</td>
                        <td>Review</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($revs as $rev)
                    <tr>
                        <td>{{$rev->id}}</td>
                        <td>{{$rev->vote}}</td>
                        <td>{{$rev->review}}</td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   @endsection

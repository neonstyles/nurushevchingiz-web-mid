<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;

class midController extends Controller
{
   public function index(){
       
       return view('indexer');
   } //

    public function create(){
        return view('create');
    }

    public function store(Request $request){
        $rev = new Review;
        $rev->vote = $request->get('vote');
        $rev->review = $request->get('review');
        $rev->save();
        return redirect()->route('welcome');
    }
}

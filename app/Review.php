<?php
namespace app;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'rev';
    public $timestamps = false;
    public function index()
    {
        return view('indexer');
    }

    public function user()
    {
        return  $this->belongsTo('app\midController');
    }
}

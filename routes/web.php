<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Review;
Route::get('/', function () {
    $revs = Review::all();
    return view('welcome', ['revs'=>$revs]);
})->name('welcome');
Route::get('revs', 'midController@index');
Route::get('revs.create','midController@create')->name('create');
Route::post('revs/store', 'midController@store')->name('store');
